# ansible-playbook

## Utilité

Ce dépôt git permet de déployer et configurer un serveur ftp, mysql (avec la création d'un user = name: myUser, password: azertyuiop et d'une database = name : wordPressDb), apache avec php et wordPress sur une machine à l'aide d'ansible (ansible-playbook)

## Démarrer

### Pré-requis

- Une VM démarrée autorisant la connexion ssh(connaître son adresse IP)
- Avoir ansible d'installé sur son pc (```sudo apt-get install ansible```  ou  ```sudo pip install ansible``` )

### Installation

Cloner le répertoire git à l'aide de git clone

Se rendre dans le répertoire cloné (```cd ansible-playbook```)

Modifier les fichiers suivants :

#### ansible.cfg :

Modifier la variable remote_user pour y mettre le nom d'utilisateur de votre VM

#### host_vars/machine1.yml :

Modifier la variable ansible_host pour y mettre l'adresse IP de votre VM à configurer

### Commandes à lancer

Une fois les modifications de fichiers terminées, il suffit de lancer les deux commandes suivantes dans l'ordre:

``` bash
ansible-playbook plays/ftpMysqlApachePhp.yml
```

``` bash
ansible-playbook plays/wordpress.yml
```

### Cas particulier

Si vous avez créé votre VM avec une authentification par password et non par clé publique ssh, vous obtiendrez une erreur : machine 1 UNREACHABLE.

Pour éviter cette erreur, il suffit d'ajouter ```--ask-pass``` à la fin de vos deux commandes ansible-playbook :

``` bash
ansible-playbook plays/ftpMysqlApachePhp.yml --ask-pass
```

``` bash
ansible-playbook plays/wordpress.yml --ask-pass
```

## Auteurs

- **Julien ANDRAL** - _Développeur_
